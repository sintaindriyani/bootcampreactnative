console.log("Soal No. 1 - Range\n");

function range(startNum, finishNum) {
  var number = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      number.push(i);
    }
    return number;
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      number.push(i);
    }
    return number;
  } else {
    return -1;
  }
}

console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());
console.log("\n");

console.log("Soal No. 2 - Range with Step\n");

function rangeWithStep(startNum, finishNum, step) {
  var number = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i+=step) {
      number.push(i);
    }
    return number
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i-=step) {
      number.push(i);
    }
    return number;
  } else {
    return -1;
  }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log("\n");

console.log("Soal No. 3 - Sum of Range\n");

function sum(startNum = 0, finishNum, step = 1) {
  var total = 0;
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i+=step) {
      total = total + i;
    }
    return total;
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i-=step) {
      total = total + i;
    }
    return total;
  } else {
    return startNum;
  }
}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
console.log("\n");

console.log("Soal No. 4 - Array Multidimensi\n");

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(data) {
  console.log("Nomor ID : " + input[data][0]);
  console.log("Nama Lengkap : " + input[data][1]);
  console.log("TTL : " + input[data][2] + " " + input[data][3]);
  console.log("Hobi : " + input[data][4]);
  console.log("\n");
}

dataHandling(0);
dataHandling(1);
dataHandling(2);
dataHandling(3);

console.log("Soal No. 5 - Balik Kata\n");

function balikKata(kata) {
  var kata = kata;
  var hasil = '';
  for (var i = kata.length-1; i >= 0; i--) {
    hasil = hasil + kata[i];
  }
  return hasil;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log("\n");

console.log("Soal No. 6 - Metode Array\n");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(data) {
  var data = data;
  data[1] = data[1] + "Elsharawy";
  data[2] = "Provinsi " + data[2];
  data.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(data);

  var hasilSplit = data[3].split("/");

  switch(hasilSplit[1]) {
    case "01" :
      var bulan = "Januari";
      break;
    case "02" :
      var bulan = "Februari";
      break;
    case "03" :
      var bulan = "Maret";
      break;
    case "04" :
      var bulan = "April";
      break;
    case "05" :
      var bulan = "Mei";
      break;
    case "06" :
      var bulan = "Juni";
      break;
    case "07" :
      var bulan = "Juli";
      break;
    case "08" :
      var bulan = "Agustus";
      break;
    case "09" :
      var bulan = "September";
      break;
    case "10" :
      var bulan = "Oktober";
      break;
    case "11" :
      var bulan = "November";
      break;
    case "12" :
      var bulan = "Desember";
      break;
    default :
      var bulan = "Invalid input";
      break;
  }

  console.log(bulan);
  
  hasilSplit.sort(function (value1, value2) { return value2 - value1 } ) ;
  console.log(hasilSplit); //kadang muncul kadang tidak

  var tanggal = data[3].split("/");
  var hasilJoin = tanggal.join("-");
  console.log(hasilJoin);

  var hasilSlice = data[1].slice(0,15);
  console.log(hasilSlice);
}